FROM php:8.2

WORKDIR /var/www/html/
RUN apt-get update -yqq
RUN apt-get install libssh2-1-dev libssh2-1 ca-certificates iputils-ping mariadb-client gnupg git curl libonig-dev libcurl4-gnutls-dev libzip-dev zip unzip libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev libsasl2-dev libssl-dev libsasl2-dev libsasl2-2 libsasl2-modules-gssapi-mit libltdl-dev libltdl3-dev libltdl7 libltdl7-dev -yqq
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update -yqq
RUN apt-get install nodejs -yqq
RUN docker-php-ext-configure gd --with-freetype
RUN docker-php-ext-configure ldap --with-ldap-sasl --with-libdir=lib/x86_64-linux-gnu/
RUN docker-php-ext-install mbstring pdo_mysql curl intl gd xml zip bz2 exif pcntl bcmath mysqli
RUN pecl install ssh2
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug ssh2

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY ./php/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

EXPOSE 80
