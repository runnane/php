# php docker image


[![pipeline status](https://gitlab.com/runnane/php/badges/main/pipeline.svg)](https://gitlab.com/runnane/php/-/commits/main)
[![Latest Release](https://gitlab.com/runnane/php/-/badges/release.svg)](https://gitlab.com/runnane/php/-/releases)


```
php 8.2
php extensions: mbstring mysqli pdo_mysql curl intl gd xml zip bz2 exif pcntl bcmath
php module: xdebug ssh2
node 20
```
